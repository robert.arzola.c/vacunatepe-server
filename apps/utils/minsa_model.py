import datetime

import requests
import numpy as np
import pandas as pd
import statsmodels.api as sm
from sklearn import linear_model
from statsmodels.sandbox.regression.predstd import wls_prediction_std  

# from apps.recopilator.models import Indicator, Info
# from apps.location.models import Province, District, Region


class MinsaModel():

    def load_data(self):
        df = pd.read_csv("/tmp/datossemana.csv", dtype={'UBIGEO': str})
        df = df.drop(['Unnamed: 0'], axis=1)
        df['DPD'] =  df['DEPARTAMENTO'].map(str) + '-' + df['PROVINCIA'].map(str) + '-' + df['DISTRITO'].map(str)
        df.drop(['DEPARTAMENTO', 'PROVINCIA','DISTRITO'], axis=1, inplace=True)
        df = df.dropna()
        return df

    def minsa_date(self, df):
        fech = pd.read_csv("/tmp/Fechas.csv")
        fech = fech.drop(['Unnamed: 0'], axis=1)
        # Fechas en base general
        new_df = pd.merge(
            df,
            fech,
            how='left',
            left_on=['SEMANA'],
            right_on = ['SEMANA']
        )
        new_df['MOVILIDAD'] = new_df[
            [
                'CENTRO_COMERCIAL', 'TIENDAS_FARMACIAS','PARQUES',
                'PARADEROS','CENTRO_LABORAL'
            ]
        ].mean(axis=1)
        # Indices
        new_df['FECHA'] = pd.to_datetime(new_df['FECHA'], format='%Y-%m-%d')
        uniq_idx = new_df.DPD.unique()
        new_df.set_index(['DPD','FECHA'], inplace = True)
        return new_df, uniq_idx

    def proccess(self, new_df, uniq_idx):
        #Talla de matriz final
        count_rows = uniq_idx.size
        df_final = pd.DataFrame(
            columns=[
                'DPD','Fecha', 'Ubigeo', 'Contagiados','Proyeccion',
                'Fallecidos','Movilidad','Vacunados'
            ], index=uniq_idx)
        j = 0
        for i in range(count_rows):
            # Define zona
            #-----------------------------------------------------------------------------
            Zona = uniq_idx[i]

            # Genera Modelo
            #-----------------------------------------------------------------------------

            # Define Variables
            x = np.asanyarray(
                new_df[
                    ['VACUNADOS','FALLECIDOS','CONTAGIOS','MOVILIDAD']
                ].xs(Zona).shift(1))
            y = np.asanyarray(new_df[['CONTAGIOS']].xs(Zona))
            
            try:
                x = np.delete(x, [0,1], 0)
                y = np.delete(y, [0,1], 0)

                # Corre el Modelo
                model = sm.OLS(y, x)
                results = model.fit()

                # Genara variables importantes
                lastdate = new_df.xs(Zona).tail(1).index.item()
                lastobs  = np.asanyarray(new_df['CONTAGIOS'].xs(Zona).tail(1))
                lastx1   = np.asanyarray(new_df['VACUNADOS'].xs(Zona).tail(1))
                lastx2   = np.asanyarray(new_df['FALLECIDOS'].xs(Zona).tail(1))
                lastx3   = np.asanyarray(new_df['CONTAGIOS'].xs(Zona).tail(1))
                lastx4   = np.asanyarray(new_df['MOVILIDAD'].xs(Zona).tail(1))
                lastubigeo = np.asanyarray(new_df['UBIGEO'].xs(Zona).tail(1))
                nextobs = results.params[0]*lastx1 + results.params[1]*lastx2 + results.params[2]*lastx3 + results.params[3]*lastx4

                # Guarda datos
                df_final['DPD'][j] = Zona
                df_final['Fecha'][j]= lastdate
                df_final['Ubigeo'][j] = np.asscalar(lastubigeo)
                df_final['Contagiados'][j] = np.asscalar(lastobs)
                df_final['Proyeccion'][j] = round(np.asscalar(nextobs))
                df_final['Fallecidos'][j] = np.asscalar(lastx2)
                df_final['Movilidad'][j] = np.asscalar(lastx4)
                df_final['Vacunados'][j] = np.asscalar(lastx1)
            except:
                pass
            j = j+1

        return df_final

    def run(self):
        data_week = self.load_data()
        data_minsa, uniq_idx = self.minsa_date(data_week)
        result_model = self.proccess(data_minsa, uniq_idx)

        for i in uniq_idx:
            if result_model['Proyeccion'][i] < 0:
                result_model['Proyeccion'][i] = result_model['Contagiados'][i]

        result_model.to_csv('/tmp/Proyeccion.csv')

scraper = MinsaModel()
scraper.run()
