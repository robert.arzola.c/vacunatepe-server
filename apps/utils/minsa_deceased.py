import json

import requests
import pandas as pd

# from apps.recopilator.models import Indicator, Info
# from apps.location.models import Province, District, Region


class MinsaDeceased():
    """ Scraper to extract vaccinated indicator"""

    def __init__(self):
        self.url_file_movility = "https://cloud.minsa.gob.pe/s/Md37cjXmjT9qYSa/download"

    def get_file_minsa(self):
        response = requests.get(self.url_file_movility)

        if response.ok:
            with open('/tmp/fallecidos_covid.csv', 'wb') as f:
                f.write(response.content)

    def parser_data(self, deceaseds_file):
        FallAux = deceaseds_file[['DEPARTAMENTO','PROVINCIA','DISTRITO']]
        FallFec = deceaseds_file['FECHA_FALLECIMIENTO']
        FallFec = FallFec.values
        FallFecOut = []
        Anho = []
        Mes = []
        Dia = []
        FecFormat = []
        for i in range(len(FallFec)):
            FallFecOut += [str(FallFec[i])]
            Anho += [FallFecOut[i][0:4]]
            Mes += [FallFecOut[i][4:6]]
            Dia += [FallFecOut[i][6:8]]
            Aux = Anho[i] + '-' + Mes[i] + '-' + Dia[i]
            FecFormat += [Aux]
        FallFecOut = pd.DataFrame(FallFecOut, columns = ['Fecha'])
        FecFormat = pd.DataFrame(FecFormat,columns=['FechaF'])
        Output = FallAux.join(FallFecOut)
        Output = Output.join(FecFormat)
        Output["FECHA_FALLECIMIENTO"] = pd.to_datetime(Output.FechaF, format='%Y-%m-%d')
        return Output

    def get_data_week(self, data):
        #Damos el formato de Semana
        data['SEMANA'] = data['FECHA_FALLECIMIENTO'].dt.strftime('%Y-%U')
        # Dropeando la Fecha Larga
        data = data.drop(['FECHA_FALLECIMIENTO'], axis=1)
        # Agrupando por Dep, Prov, Dist, Semana
        data['Fecha'] = 1
        data = data.groupby(
            by=['DEPARTAMENTO', 'PROVINCIA','DISTRITO','SEMANA'],
            as_index=False
        ).sum()
        return data

    def merge_with_ubigeo(self, data):
        #Añadiendo la información de Ubigeo
        # Cargando los datos
        url = "https://raw.githubusercontent.com/JoseGallardoArb/Data/main/ubigeo.csv" 
        location = pd.read_csv(url,dtype={'Ubigeo': str})
        # Ordenando
        location = location[['Departamento','Provincia','Distrito','Ubigeo']]
        # Renombrando para combinar
        location = location.rename(
            columns={
                'Departamento': 'DEPARTAMENTO',
                'Provincia':'PROVINCIA',
                'Distrito':'DISTRITO',
                'Ubigeo':'UBIGEO'
            }
        )
        return location

    def save_data_deceased(self, data_week, data_ubigeo):
        # Combinando
        new_df = pd.merge(
            data_week,
            data_ubigeo,
            how='left',
            left_on=['DEPARTAMENTO', 'PROVINCIA','DISTRITO'],
            right_on = ['DEPARTAMENTO', 'PROVINCIA','DISTRITO']
        )
        # Ordenando
        new_df = new_df[
            ['DEPARTAMENTO', 'PROVINCIA','DISTRITO','UBIGEO','SEMANA','Fecha']
        ]
        new_df.to_csv('/tmp/FallecidosUS.csv')

    def save_data_dates(self, data):
        # Damos el formato de Semana
        f_name = 'FECHA_FALLECIMIENTO'
        data['SEMANA'] = data[f_name].dt.strftime('%Y-%U')

        # Damos el formato de Semana
        data["FECHA"] = pd.to_datetime(data.FECHA_FALLECIMIENTO, format='%Y-%m-%d')
        Aux = data.loc[:, (f_name, 'DEPARTAMENTO', 'PROVINCIA', 'FECHA')]
        Aux['SEMANA'] = Aux['FECHA'].dt.strftime('%Y-%U')
        Aux['DIASEM'] = Aux['FECHA'].dt.dayofweek
        Aux = Aux[Aux['DIASEM']==0]
        Aux = Aux[[f_name,'SEMANA','FECHA']]
        Aux = Aux.drop_duplicates()
        Aux.to_csv('/tmp/Fechas.csv')

    def run(self):
        self.get_file_minsa()
        deceaseds_file = pd.read_csv('/tmp/fallecidos_covid.csv', sep=";")
        data = self.parser_data(deceaseds_file)
        data_week = self.get_data_week(data)
        data_ubigeo = self.merge_with_ubigeo(data_week)
        self.save_data_deceased(data_week, data_ubigeo)
        self.save_data_dates(data)


scraper = MinsaDeceased()
scraper.run()
