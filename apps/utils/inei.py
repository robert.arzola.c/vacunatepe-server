import openpyxl

from apps.recopilator.models import Indicator, Info
from apps.location.models import District, Region, Province


class IneiPeru:

    def extract_data(self):
        wb = openpyxl.load_workbook("./apps/assets/CensoPob.xlsx")
        ws = wb.active
        indicator = Indicator.objects.get(name="Población")
        for row in ws.iter_rows():
            if row[0].value == "ubigeo":
                continue

            region = Region.objects.get(code=row[0].value[:2])
            province = Province.objects.get(code=row[0].value[:4])
            district = District.objects.get(code=row[0].value)

            obj = Info(indicator=indicator, region=region, province=province,
                        district=district, value=str(row[1].value))
            obj.save()
