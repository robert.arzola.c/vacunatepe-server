import json
import zipfile

import requests
import pandas as pd


from apps.utils.minsa_contagion import MinsaContagion
from apps.utils.minsa_deceased import MinsaDeceased
from apps.utils.minsa_vaccines import MinsaVaccines
from apps.utils.google_movility import GoogleMovility


class MinsaMerge():
    """ Scraper to extract vaccinated indicator"""

    def __init__(self):
        self.session = requests.Session()

    def get_data_contagion(self):
        data = pd.read_csv("/tmp/contagiosUS.csv", dtype={'UBIGEO': str})
        data = data.drop(['Unnamed: 0'], axis=1)
        data = data.rename(columns={'UUID': 'CONTAGIOS'})
        return data

    def get_data_vaccines(self):
        data = pd.read_csv("/tmp/vacunaUS.csv", dtype={'UBIGEO': str})
        data = data.drop(['Unnamed: 0'], axis=1)
        data = data.rename(columns={'UUID': 'VACUNADOS'})
        return data

    def get_data_deceased(self):
        data = pd.read_csv("/tmp/FallecidosUS.csv", dtype={'UBIGEO': str})
        data = data.drop(['Unnamed: 0'], axis=1)
        data = data.rename(columns={'Fecha': 'FALLECIDOS'})
        return data

    def get_data_movility(self):
        data = pd.read_csv("/tmp/Movilidad.csv", dtype={'UBIGEO': str})
        data = data.drop(['Unnamed: 0'], axis=1)
        data = data.rename(
            columns={
                'parks_percent_change_from_baseline':'PARQUES',
                'workplaces_percent_change_from_baseline':'CENTRO_LABORAL',
                'transit_stations_percent_change_from_baseline':'PARADEROS',
                'grocery_and_pharmacy_percent_change_from_baseline':'TIENDAS_FARMACIAS',
                'retail_and_recreation_percent_change_from_baseline': 'CENTRO_COMERCIAL',
            }
        )
        data = data[
            ['DEPARTAMENTO', 'PROVINCIA','DISTRITO','UBIGEO',
            'SEMANA','CENTRO_COMERCIAL','TIENDAS_FARMACIAS',
            'PARQUES','PARADEROS','CENTRO_LABORAL']
        ]
        return data

    def merge_vaccines_with_contagion(self, data_vaccines, data_contagion):
        data = pd.merge(
            data_contagion,
            data_vaccines,
            how='left',
            left_on=['DEPARTAMENTO', 'PROVINCIA','DISTRITO','UBIGEO','SEMANA'],
            right_on = ['DEPARTAMENTO', 'PROVINCIA','DISTRITO','UBIGEO','SEMANA']
        )
        data['VACUNADOS'] = data['VACUNADOS'].fillna(0)
        return data

    def merge_preview(self, m_contagion_vaccines, data_movilidad):
        data = pd.merge(
            m_contagion_vaccines,
            data_movilidad,
            how='left',
            left_on=['DEPARTAMENTO', 'PROVINCIA','DISTRITO','UBIGEO','SEMANA'],
            right_on = ['DEPARTAMENTO', 'PROVINCIA','DISTRITO','UBIGEO','SEMANA']
        )
        return data

    def merge_all(self, data_m_preview, data_deceased):
        data = pd.merge(
            data_m_preview,
            data_deceased,
            how='left',
            left_on=['DEPARTAMENTO', 'PROVINCIA','DISTRITO','UBIGEO','SEMANA'],
            right_on = ['DEPARTAMENTO', 'PROVINCIA','DISTRITO','UBIGEO','SEMANA']
        )
        data['FALLECIDOS'] = data['FALLECIDOS'].fillna(0)
        return data

    def run(self):
        data_contagion = self.get_data_contagion()
        data_vaccines = self.get_data_vaccines()
        m_contagion_vaccines = self.merge_vaccines_with_contagion(
            data_vaccines, data_contagion
        )
        data_movilidad = self.get_data_movility()
        merge_preview = self.merge_preview(m_contagion_vaccines, data_movilidad)
        data_deceased = self.get_data_deceased()
        data = self.merge_all(merge_preview, data_deceased)
        data.to_csv('/tmp/datossemana.csv')


scraper_minsa = MinsaContagion()
scraper_minsa.run()

scraper_deceased = MinsaDeceased()
scraper_deceased.run()

scraper_vaccines = MinsaVaccines()
scraper_vaccines.run()

scraper_google_movility = GoogleMovility()
scraper_google_movility.run()

scraper_minsa_merge = MinsaMerge()
scraper_minsa_merge.run()
