import json
import zipfile

import requests
import pandas as pd

# from apps.recopilator.models import Indicator, Info
# from apps.location.models import Province, District, Region


class GoogleMovility():
    """ Scraper to extract vaccinated indicator"""

    def __init__(self):
        self.session = requests.Session()
        self.url_file_movility = "https://www.gstatic.com/covid19/mobility/Region_Mobility_Report_CSVs.zip"

    def get_files_google(self):
        response = requests.get(self.url_file_movility)

        if response.ok:
            with open('/tmp/report_general_movility.zip', 'wb') as f:
                f.write(response.content)

        with zipfile.ZipFile("/tmp/report_general_movility.zip", 'r') as zip_ref:
            zip_ref.extractall("/tmp/report_movility")
            return "/tmp/report_movility/"

    def process_data(self):
        directory_files = self.get_files_google()
        file_report = "2020_PE_Region_Mobility_Report.csv"
        path_file_report = directory_files + file_report
        Input = pd.read_csv(path_file_report)

        #Eliminamos las columnas que no vamos a utilizar
        DF = Input[
            ['date','sub_region_1', 'sub_region_2',
            'retail_and_recreation_percent_change_from_baseline',
            'grocery_and_pharmacy_percent_change_from_baseline',
            'parks_percent_change_from_baseline',
            'transit_stations_percent_change_from_baseline',
            'workplaces_percent_change_from_baseline']]

        #FallecidosOut['SEMANA'] = FallecidosOut['FECHA_FALLECIMIENTO'].dt.strftime('%Y-%U')
        #Eliminamos las observaciones que no están registradas en ningún departamento
        def DropNaProv(Input):
            Input = Input.fillna(0)
            Variables  = list(Input)
            Variables[1] = 'DEPARTAMENTO'
            Variables[2] = 'PROVINCIA'
            Aux = Input.values
            AuxOut = []
            for i in range(len(Aux)):
                if Aux[i][2] == 0:
                    continue
                else:
                    V = []
                    V += [Aux[i][0]]
                    V += [Aux[i][1].upper()]
                    V += [Aux[i][2].upper()]
                    V += [float(Aux[i][3])]
                    V += [float(Aux[i][4])]
                    V += [float(Aux[i][5])]
                    V += [float(Aux[i][6])]
                    V += [float(Aux[i][7])]
                    AuxOut += [V]
            Output = pd.DataFrame(AuxOut, columns = Variables)  
            return Output
        return DropNaProv(DF)

    def parser_data(self):
        data = self.process_data()
        #Damos el formato de Semana
        data["date"] = pd.to_datetime(data.date, format='%Y-%m-%d')
        data['SEMANA'] = data['date'].dt.strftime('%Y-%U')
        # Dropeando la Fecha Larga
        data = data.drop(['date'], axis=1)
        # Agrupando por Dep, Prov, Dist, Semana
        data = data.groupby(by=['DEPARTAMENTO','PROVINCIA','SEMANA'], as_index=False).mean()
        return data

    def get_data_location(self):
        # Cargando los datos
        url = "https://raw.githubusercontent.com/JoseGallardoArb/Data/main/ubigeo.csv" 
        location = pd.read_csv(url, dtype={'Ubigeo': str})
        # Ordenando
        location = location[['Departamento','Provincia','Distrito','Ubigeo']]
        # Renombrando para combinar
        location = location.rename(
            columns={
                'Departamento': 'DEPARTAMENTO',
                'Provincia':'PROVINCIA',
                'Distrito':'DISTRITO',
                'Ubigeo':'UBIGEO'
            }
        )
        return location

    def run(self):
        data = self.parser_data()
        location = self.get_data_location()

        #Arreglamos los Datos de Provincia y Departamento Mal LLamados en la BASE
        data['PROVINCIA'] = data['PROVINCIA'].str.replace(' PROVINCE', '', regex=True)
        data['DEPARTAMENTO'] = data['DEPARTAMENTO'].str.replace(' REGION', '', regex=True)
        data['DEPARTAMENTO'] = data['DEPARTAMENTO'].str.replace('METROPOLITAN MUNICIPALITY OF ', '', regex=True)

        # Combinando
        new_df = pd.merge(
            data,
            location,
            how='left', 
            left_on=['DEPARTAMENTO', 'PROVINCIA'],
            right_on = ['DEPARTAMENTO', 'PROVINCIA']
        )

        #Exportamos la base a csv
        new_df.to_csv('/tmp/Movilidad.csv')


scraper = GoogleMovility()
scraper.run()
