import datetime

import requests
import pandas as pd

# from apps.recopilator.models import Indicator, Info
# from apps.location.models import Province, District, Region


class MinsaVaccines():
    """ Scraper to extract vaccinated indicator"""

    def __init__(self):
        self.minsa_file = "https://cloud.minsa.gob.pe/s/ZgXoXqK2KLjRLxD/download"

    def get_vaccines_by_week(self, df_original):
        pass

    def get_data_ubigeo(self):
        pass

    def merge_data(self, data_by_week, data_location):
        pass

    def run(self):
        pass


scraper = MinsaVaccines()
scraper.run()
