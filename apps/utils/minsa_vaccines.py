import datetime

import requests
import pandas as pd

# from apps.recopilator.models import Indicator, Info
# from apps.location.models import Province, District, Region


class MinsaVaccines():
    """ Scraper to extract vaccinated indicator"""

    def __init__(self):
        self.minsa_file = "https://cloud.minsa.gob.pe/s/ZgXoXqK2KLjRLxD/download"

    def get_vaccines_by_week(self, df_original):
        df = df_original
        # Dando formato de fecha a la fecha de resultado de la prueba del covid
        df["FECHA_VACUNACION"] = df["FECHA_VACUNACION"].astype(int)
        df["FECHA_VACUNACION"] = pd.to_datetime(df['FECHA_VACUNACION'], format='%Y%m%d')
        # Drop a las observaciones sin fecha de resultado
        df.dropna(subset=['FECHA_VACUNACION'], axis=0, inplace=True)
        # Drop a las variables poco relevantes por el momento
        df = df.drop(['FECHA_CORTE','GRUPO_RIESGO','EDAD','FABRICANTE','SEXO','DOSIS'], axis=1)
        # Reordenamiento de variables
        df = df[['DEPARTAMENTO', 'PROVINCIA','DISTRITO','FECHA_VACUNACION','UUID']]
        # Ordenando por Dep, Prov, Dist y Fecha
        df = df.sort_values(by=['DEPARTAMENTO', 'PROVINCIA','DISTRITO','FECHA_VACUNACION'])
        # Agrupando las cantidad de UUID = Personas contagiadas
        df = df.groupby(by=['DEPARTAMENTO', 'PROVINCIA','DISTRITO','FECHA_VACUNACION'], as_index=False).count()
     
        # Dando el formato de semana
        df['SEMANA'] = df["FECHA_VACUNACION"].dt.strftime('%Y-%U')
        # Drop fecha larga
        df = df.drop(["FECHA_VACUNACION"], axis=1)
        # Agrupando por Dep, Prov, Dist, Semana
        df = df.groupby(by=['DEPARTAMENTO', 'PROVINCIA','DISTRITO','SEMANA'], as_index=False).sum()

        return df

    def get_data_ubigeo(self):
        # Cargando los datos
        url = "https://raw.githubusercontent.com/vacunatepe/automatcoviddata/main/ubigeo.csv" 
        location = pd.read_csv(url,dtype={'Ubigeo': str})

        # Ordenando
        location = location[['Departamento','Provincia','Distrito','Ubigeo']]
        # Renombrando para combinar
        location = location.rename(columns={'Departamento': 'DEPARTAMENTO','Provincia':'PROVINCIA','Distrito':'DISTRITO','Ubigeo':'UBIGEO'})
        return location

    def merge_data(self, data_by_week, data_location):
        # Combinando
        new_df = pd.merge(
            data_by_week, data_location,
            how='left',
            left_on=['DEPARTAMENTO', 'PROVINCIA','DISTRITO'],
            right_on = ['DEPARTAMENTO', 'PROVINCIA','DISTRITO']
        )
        # Ordenando
        new_df = new_df[['DEPARTAMENTO', 'PROVINCIA','DISTRITO','UBIGEO','SEMANA','UUID']]
        return new_df

    def run(self):
        df_original = pd.read_csv(self.minsa_file, sep=",")
        df_original.head()

        data_by_week = self.get_vaccines_by_week(df_original)
        data_location = self.get_data_ubigeo()

        data_merge = self.merge_data(data_by_week, data_location)
        data_merge.to_csv('/tmp/vacunaUS.csv')


scraper = MinsaVaccines()
scraper.run()
