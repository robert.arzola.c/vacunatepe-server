import csv

from apps.recopilator.models import Indicator, Info
from apps.location.models import District, Region, Province


class MinsaProccesModel():
    def extract_data(self):
        with open('/tmp/Proyeccion.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            headers = next(csv_reader)

            indicator = Indicator.objects.get(name="Proyección")
            for row in csv_reader:
                print('row_date: ', row[2])
                print('row_ubigeo: ', row[3])
                print('row_proyection: ', row[5])
                region = Region.objects.get(code=row[3].value[:2])
                province = Province.objects.get(code=row[3].value[:4])
                district = District.objects.get(code=row[3].value)

                obj = Info(indicator=indicator, region=region, province=province,
                            district=district, value=str(row[5]))
                obj.save()

proccesor = MinsaProccesModel()
proccesor.extract_data()
