import json

import requests

from apps.recopilator.models import Indicator, Info
from apps.location.models import Province, District, Region


class MinsaPeru():
    """ Scraper to extract vaccinated indicator"""

    def __init__(self):
        self.session = requests.Session()
        self.api = "https://gis.minsa.gob.pe/WebApiRep2/api/"

    def get_data_region(self):
        path = "Departamento/ListarVacunadosPublico"
        url = self.api + path

        data_json = {"DisaCodigo":0, "IdDepartamento":""}
        response = self.session.post(url, json=data_json)

        data = response.json()["Data"]
        indicator = Indicator.objects.get(name="Vacunados")
        indicator_2 = Indicator.objects.get(name="Meta | Vacunados")

        for item in data:
            region = Region.objects.get(code=item["IdDepartamento"])
            obj = Info(indicator=indicator, region=region,
                        value=item["Vacunados"])
            obj.save()

            obj_2 = Info(indicator=indicator_2, region=region,
                        value=item["Meta"])
            obj_2.save()

    def get_data_provinces(self):
        path = "Provincia/ListarVacunadosPublico"
        url = self.api + path

        data_json = {"DisaCodigo":0, "IdDepartamento":"", "IdProvincia":""}
        response = self.session.post(url, json=data_json)

        data = response.json()["Data"]
        indicator = Indicator.objects.get(name="Vacunados")
        indicator_2 = Indicator.objects.get(name="Meta | Vacunados")

        for item in data:
            code = item["IdDepartamento"] + item["IdProvincia"]
            region = Region.objects.get(code=item["IdDepartamento"])
            province = Province.objects.get(region=region, code=code)
            obj = Info(indicator=indicator, region=region, province=province,
                        value=item["Vacunados"])
            obj.save()

            obj_2 = Info(indicator=indicator_2, region=region,
                        value=item["Meta"])
            obj_2.save()

    def get_data_districts(self):
        path = "Distrito/ListarVacunadosPublico"
        url = self.api + path

        data_json = {"DisaCodigo":0, "IdDepartamento":"",
                        "IdProvincia":"", "IdDistrito":""}
        response = self.session.post(url, json=data_json)

        data = response.json()["Data"]
        indicator = Indicator.objects.get(name="Vacunados")
        indicator_2 = Indicator.objects.get(name="Meta | Vacunados")

        for item in data:
            code_province = item["IdDepartamento"] + item["IdProvincia"]
            code_district = code_province + item["IdDistrito"]

            region = Region.objects.get(code=item["IdDepartamento"])
            province = Province.objects.get(region=region, code=code_province)
            district = District.objects.get(
                province=province, code=code_district)

            obj = Info(indicator=indicator, region=region, province=province,
                        district=district, value=item["Vacunados"])
            obj.save()

            obj_2 = Info(indicator=indicator_2, region=region,
                        value=item["Meta"])
            obj_2.save()
