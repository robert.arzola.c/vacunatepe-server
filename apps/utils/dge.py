import json
from io import BytesIO

import requests
import pytesseract
from PIL import Image
from datetime import datetime

from apps.location.models import Region
from apps.recopilator.models import Indicator, Info


class DegPeru():
    """ Scraper to extract vaccinated indicator"""

    def __init__(self):
        self.session = requests.Session()
        self.website = "https://www.dge.gob.pe/portal"

    def gen_date(self):
        now = datetime.now()
        time = now.strftime("%d%m%y")
        return time

    def extract_data(self):
        # date = self.gen_date()
        date = "030321"
        path = f"/docs/tools/coronavirus/coronavirus{date}.pdf"
        url = self.website + path

        response = self.session.get(url, allow_redirects=True)
        print('response: ', response)
        report_pdf = Image.open(BytesIO(response.text))
        text = pytesseract.image_to_string(report_pdf)
        print('text: ', text)
