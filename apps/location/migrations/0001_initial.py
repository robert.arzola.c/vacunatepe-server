# Generated by Django 3.1.5 on 2021-03-06 04:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.PositiveSmallIntegerField()),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name_plural': 'Regions',
            },
        ),
        migrations.CreateModel(
            name='Province',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.PositiveSmallIntegerField()),
                ('name', models.CharField(max_length=50)),
                ('region', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='provinces', to='location.region')),
            ],
            options={
                'verbose_name_plural': 'Provinces',
            },
        ),
        migrations.CreateModel(
            name='District',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.BigIntegerField(default=0)),
                ('name', models.CharField(max_length=50)),
                ('province', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='districts', to='location.province')),
            ],
            options={
                'verbose_name_plural': 'Districts',
            },
        ),
    ]
