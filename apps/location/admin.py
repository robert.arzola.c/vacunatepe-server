from django.contrib import admin
from apps.location.models import Region, Province, District


class RegionAdmin(admin.ModelAdmin):
    list_display = ("code", "name")


class ProvinceAdmin(admin.ModelAdmin):
    list_display = ("code", "name", "region")
    search_fields = ('region__name', )


class DistrictAdmin(admin.ModelAdmin):
    list_display = ("code", "name", "province")
    search_fields = ('province__name', )


admin.site.register(Region, RegionAdmin)
admin.site.register(Province, ProvinceAdmin)
admin.site.register(District, DistrictAdmin)
