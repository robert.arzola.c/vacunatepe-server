from django.db import models
from django.utils.translation import gettext as _


class Region(models.Model):
    code = models.CharField(_("Code"), max_length=7, blank=True, null=True)
    name = models.CharField(_("Name"), max_length=50, blank=True, null=True)

    class Meta:
        verbose_name_plural = _("Regions")

    def __str__(self):
        return self.name


class Province(models.Model):
    region = models.ForeignKey(
        Region, related_name='provinces',
        blank=True, null=True, on_delete=models.PROTECT)
    code = models.CharField(_("Code"), max_length=7, blank=True, null=True)
    name = models.CharField(_("Name"), max_length=50, blank=True, null=True)

    class Meta:
        verbose_name_plural = _("Provinces")

    def __str__(self):
        return self.name


class District(models.Model):
    province = models.ForeignKey(
        Province, related_name='districts',
        blank=True,null=True, on_delete=models.PROTECT)
    code = models.CharField(_("Code"), max_length=7, blank=True, null=True)
    name = models.CharField(_("Name"), max_length=50, blank=True, null=True)

    class Meta:
        verbose_name_plural = _("Districts")

    def __str__(self):
        return self.name
