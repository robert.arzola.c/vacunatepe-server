import traceback

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from django.utils.translation import gettext as _

from apps.utils.dge import DegPeru
from apps.utils.inei import IneiPeru
from apps.utils.minsa import MinsaPeru


class ExtractMinsaData(APIView):
    """
    Load scraper minsa.
    """

    def get(self, request, format=None):
        try:
            scraper = MinsaPeru()
            scraper.get_data_region()
            scraper.get_data_provinces()
            scraper.get_data_districts()
            msg = {"succes": True, "message": _("Successful execution")}
            return Response(msg, status=status.HTTP_200_OK)
        except:
            e = traceback.format_exc()
            print('e: ', e)
            msg = {"succes": False, "error": "Problem in platform."}
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ExtractDgeData(APIView):
    """
    Load scraper dge by region.
    """

    def get(self, request, format=None):
        try:
            scraper = DegPeru()
            scraper.extract_data()
            msg = {"succes": True, "message": _("Successful execution")}
            return Response(msg, status=status.HTTP_200_OK)
        except:
            e = traceback.format_exc()
            print('e: ', e)
            msg = {"succes": False, "error": "Problem in platform."}
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ExtractIneiData(APIView):
    """
    Load scraper dge by region.
    """

    def get(self, request, format=None):
        try:
            scraper = IneiPeru()
            scraper.extract_data()
            msg = {"succes": True, "message": _("Successful execution")}
            return Response(msg, status=status.HTTP_200_OK)
        except:
            e = traceback.format_exc()
            print('e: ', e)
            msg = {"succes": False, "error": "Problem in platform."}
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
