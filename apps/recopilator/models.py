from django.db import models
from django.utils.translation import gettext as _

from apps.location.models import Region, Province, District


class Source(models.Model):
    name = models.CharField(_("Name"), max_length=250, blank=True, null=True)
    detail = models.CharField(_("Detail"), max_length=250, blank=True, null=True)
    website = models.URLField(_("Website"), max_length=250, blank=True, null=True)

    class Meta:
        verbose_name = _("Source")
        verbose_name_plural = _("Sources")

    def __str__(self):
        return self.name


class Indicator(models.Model):
    MONEY = "MON"
    INTEGER = "INT"
    PERCENTAGE = "PER"

    ANNUAL = "ANN"
    MONTHLY = "MON"
    DIARY = "DIA"

    TYPE_UNIT_CHOICES = [
        (MONEY, _("Money")),
        (INTEGER, _("Integer")),
        (PERCENTAGE, _("Porcentage")),
    ]

    TYPE_RANGE_TIME_CHOICES = [
        (ANNUAL, _("Annual")),
        (MONTHLY, _("Monthly")),
        (DIARY, _("Diary")),
    ]

    type = models.CharField(
        _("Type"), max_length=3, choices=TYPE_RANGE_TIME_CHOICES)
    name = models.CharField(_("Name"), max_length=250, blank=True, null=True)
    source= models.ForeignKey(
        Source, on_delete=models.PROTECT,
        verbose_name=_("Source"), blank=True, null=True)
    unit = models.CharField(
        _("Unit"), max_length=3, choices=TYPE_UNIT_CHOICES, default="M")

    class Meta:
        verbose_name = _("Indicator")
        verbose_name_plural = _("Indicators")

    def __str__(self):
        return self.name


class Info(models.Model):
    date_source = models.DateField(_("Date Source"), blank=True, null=True)
    date_scraped = models.DateField(_("Date scraping"), auto_now=True)
    indicator = models.ForeignKey(
        Indicator, on_delete=models.PROTECT,
        verbose_name=_("Indicator"), blank=True, null=True)
    region = models.ForeignKey(
        Region, blank=True,
        null=True, on_delete=models.PROTECT, verbose_name=_("Region"))
    province = models.ForeignKey(
        Province, blank=True,
        null=True, on_delete=models.PROTECT, verbose_name=_("Province"))
    district = models.ForeignKey(
        District, blank=True,
        null=True, on_delete=models.PROTECT, verbose_name=_("District"))
    value = models.CharField(_("Value"), max_length=50, blank=True, null=True)

    class Meta:
        verbose_name = _("information")
        verbose_name_plural = _("information")

    def __str__(self):
        return u'Info - %s' % self.pk
