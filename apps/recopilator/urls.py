from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from apps.recopilator.views import ExtractMinsaData, ExtractDgeData, ExtractIneiData


urlpatterns = [
    path('minsa/extract-data', ExtractMinsaData.as_view()),
    path('inei/extract-data', ExtractIneiData.as_view()),
    path('dge/extract-data', ExtractDgeData.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
