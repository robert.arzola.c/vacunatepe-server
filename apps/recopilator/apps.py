from django.apps import AppConfig


class RecopilatorConfig(AppConfig):
    name = 'recopilator'
