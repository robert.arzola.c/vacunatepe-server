from django.contrib import admin
from apps.recopilator.models import Source, Indicator, Info


class SourceAdmin(admin.ModelAdmin):
    list_display = ('name', 'detail', 'website')


class IndicatorsAdmin(admin.ModelAdmin):
    list_display = ('name', 'source', 'type', 'unit')
    list_filter = ('source',)
    fieldsets = (
        (None, {
            'fields': ('name', 'source', 'type', 'unit')
        }),
    )


class InfoAdmin(admin.ModelAdmin):
    list_display = (
        'region', 'province', 'district', 'get_source', 'indicator',
        'value', 'date_source', 'date_scraped')
    actions = ["export_as_csv"]
    list_filter = ('indicator__source', 'indicator',)
    search_fields = (
        'region__name', 'province__name', 'district__name')

    def get_source(self, obj):
        return obj.indicator.source
    get_source.short_description = 'Fuente'


admin.site.register(Source, SourceAdmin)
admin.site.register(Indicator, IndicatorsAdmin)
admin.site.register(Info, InfoAdmin)
